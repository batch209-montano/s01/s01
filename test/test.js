const { assert } = require('chai');
const { newUser } = require('../index.js');

//describe give structure to your test suite
describe('Test newUserObject', () =>{

    //unit test
    it('Assert newUser type is object', () =>{

        assert.equal(typeof(newUser),'object')

    })

    it('Assert newUser.email is type string', () => {
       
        assert.equal(typeof(newUser.email), 'string')

    });

    //Mini-Activity:
    //Create a test that asserts that newUser.email is not equal to undefined.

    //In your personal gitlab, create a new folder called b209
    //Create a new online repo called s1
    //Initialize your local s1 as a git repo, add and commit your updates with the following commit message: "Add s1 mini-activity"

    //link your repo to boodle

    

    it('Assert newUser.email is not type undefined', () => {

        //.equal = testing for equality ==
        assert.notEqual(typeof(newUser.email), 'undefined')

    });

    

    it('Assert newUser.password is a string', () =>{

        //.notEqual = testing for inequality !=
        assert.equal(typeof(newUser.password),'string')

    });

    
    it('Assert that the number of characters in newUser.password is at least 16', () =>{

        //.isAtLeast = testing for greater than or equal to >=
        assert.isAtLeast(newUser.password.length,16)
        
    });

    //Activity Instructions:
        
       // 1. Assert the newUser firstName is a string.

       it('Assert newUser.firstName is a string', () =>{

        assert.equal(typeof(newUser.firstName),'string')
        
       });

       // 2. Assert the newUser lastName is a string.

       it('Assert newUser.lastName is a string', () =>{

        assert.equal(typeof(newUser.lastName),'string')
        
       });

       // 3. Assert the newUser firstName is not undefined.

       it('Assert newUser.firstName is not undefined', () =>{

        assert.notEqual(typeof(newUser.firstName),undefined)
        
       });

       // 4. Assert the newUser lastName is not undefined.

       it('Assert newUser.lastName is a string', () =>{

        assert.notEqual(typeof(newUser.lastName),'undefined')
        
       });

       // 5. Assert the newUser age is at least 18.

       it('Assert the newUser.age is at least 18', () =>{

        assert.isAtLeast(newUser.age,18)
        
       });

       // 6. Assert the newUser age type is a number.

       it('Assert the newUser.age is a number', () =>{

        assert.equal(typeof(newUser.age), 'number')
        
       });

       // 7. Assert the newUser contact number type is a string.

       it('Assert the newUser.contactNumber is a string', () =>{

        assert.equal(typeof(newUser.contactNumber), 'string')
        
       });

       // 8. Assert the newUser contact number is at least 11  characters.

       it('Assert that the number of characters in newUser.contactNumber is at least 11', () =>{

        assert.isAtLeast(newUser.contactNumber.length, 11)
        
       });

       // 9. Assert the newUser batch number type  is a number.
      
       it('Assert the newUser.batchNumber is a number', () =>{

        assert.equal(typeof(newUser.batchNumber), 'number')
        
       });

       // 10. Assert the newUser fbatch number is not undefined.

       it('Assert the newUser.batchNumber is not undefined', () =>{

        assert.notEqual(typeof(newUser.batchNumber), undefined)
        
       });
         

});
